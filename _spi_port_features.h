#ifndef SPI_PORT_FEATURES_H
#define SPI_PORT_FEATURES_H

/* пример файла, описывающиего возможности конкретного порта */

/* Максимальное количество аппаратно управляемых CS на один интерефейс SPI.
 * Если 0, то аппаратное управление CS не поддерживается */
#define SPI_PORT_CS_HWCTL_MAX_CNT    1
/* Максимальный размер слова для обмена с устройством */
#define SPI_PORT_MAX_DATA_LEN        16
/* Поддерживает ли порт и реализован ли режим работы через DMA */
#define SPI_PORT_SUPPORT_DMA         0

/* признак, разрешена ли работа и настройка интерфейса с номером iface_num */
#define SPI_PORT_IFACE_ENABLE(iface_num)   1

/* определение констант для каждого поддерживаемое портом SPI интерфейса для
 * задания в описании устройства */
//#define SPI_IFACEx_NUM  x


#endif // _SPI_PORT_FEATURES_H
