#ifndef SPI_PORT_FEATURES_H
#define SPI_PORT_FEATURES_H


#include "chip.h"
#include "spi_config.h"

#define SPI_PORT_CS_HWCTL_MAX_CNT   1
#define SPI_PORT_SUPPORT_DMA        0
#define SPI_PORT_MAX_DATA_LEN       32

#define SPI_PORT_MAX_IFACE_CNT      6

#define SPI_IFACE1_NUM         0
#define SPI_IFACE2_NUM         1
#define SPI_IFACE3_NUM         2
#define SPI_IFACE4_NUM         3
#define SPI_IFACE5_NUM         4
#define SPI_IFACE6_NUM         5


#ifdef SPI_CFG_IFACE1_EN
    #define SPI_IFACE1_EN SPI_CFG_IFACE1_EN
#else
    #define SPI_IFACE1_EN 0
#endif

#ifdef SPI_CFG_IFACE2_EN
    #define SPI_IFACE2_EN SPI_CFG_IFACE2_EN
#else
    #define SPI_IFACE2_EN 0
#endif

#if defined SPI_CFG_IFACE3_EN
    #define SPI_IFACE3_EN SPI_CFG_IFACE3_EN
#else
    #define SPI_IFACE3_EN 0
#endif

#if defined SPI_CFG_IFACE4_EN
    #define SPI_IFACE4_EN SPI_CFG_IFACE4_EN
#else
    #define SPI_IFACE4_EN 0
#endif

#if defined SPI_CFG_IFACE5_EN
    #define SPI_IFACE5_EN SPI_CFG_IFACE5_EN
#else
    #define SPI_IFACE5_EN 0
#endif

#if defined SPI_CFG_IFACE6_EN
    #define SPI_IFACE6_EN SPI_CFG_IFACE6_EN
#else
    #define SPI_IFACE6_EN 0
#endif


#endif // SPI_PORT_FEATURES_H
