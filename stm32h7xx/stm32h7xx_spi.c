#include "spi_port.h"
#include "chip.h"
#include <stddef.h>

typedef struct {
    CHIP_REGS_SPI_T *regs;
    t_chip_per_id  per_id;
    unsigned  ker_freq;
    unsigned  fifo_len;
    unsigned  max_dlen;
} t_spi_iface_info;

static const t_spi_iface_info f_ifaces[SPI_PORT_MAX_IFACE_CNT] = {
#if SPI_IFACE1_EN
    {CHIP_REGS_SPI1, CHIP_PER_ID_SPI1, CHIP_CLK_SPI1_KER_FREQ, CHIP_SPI1_FIFO_SIZE, CHIP_SPI1_WORD_MAX_SIZE},
#else
    {NULL, 0, 0, 0, 0},
#endif
#if SPI_IFACE2_EN
    {CHIP_REGS_SPI2, CHIP_PER_ID_SPI2, CHIP_CLK_SPI2_KER_FREQ, CHIP_SPI2_FIFO_SIZE, CHIP_SPI2_WORD_MAX_SIZE},
#else
    {NULL, 0, 0, 0, 0},
#endif
#if SPI_IFACE3_EN
    {CHIP_REGS_SPI3, CHIP_PER_ID_SPI3, CHIP_CLK_SPI3_KER_FREQ, CHIP_SPI3_FIFO_SIZE, CHIP_SPI3_WORD_MAX_SIZE},
#else
    {NULL, 0, 0, 0, 0},
#endif
#if SPI_IFACE4_EN
    {CHIP_REGS_SPI4, CHIP_PER_ID_SPI4, CHIP_CLK_SPI4_KER_FREQ,  CHIP_SPI4_FIFO_SIZE, CHIP_SPI4_WORD_MAX_SIZE},
#else
    {NULL, 0, 0, 0, 0},
#endif
#if SPI_IFACE5_EN
    {CHIP_REGS_SPI5, CHIP_PER_ID_SPI5, CHIP_CLK_SPI5_KER_FREQ,  CHIP_SPI5_FIFO_SIZE, CHIP_SPI5_WORD_MAX_SIZE},
#else
    {NULL, 0, 0, 0, 0},
#endif
#if SPI_IFACE6_EN
    {CHIP_REGS_SPI6, CHIP_PER_ID_SPI6, CHIP_CLK_SPI6_KER_FREQ,  CHIP_SPI6_FIFO_SIZE, CHIP_SPI6_WORD_MAX_SIZE},
#else
    {NULL, 0, 0, 0, 0},
#endif
};

static uint8_t f_onfly_bytes[SPI_PORT_MAX_IFACE_CNT];

static const uint16_t f_clk_div[] = {2, 4, 8, 16, 32, 64, 128, 256};



t_spi_err spi_port_iface_init(uint8_t iface_num) {
    t_spi_err err = SPI_ERR_OK;
    const t_spi_iface_info *iface = &f_ifaces[iface_num];

    if ((iface->regs == NULL) || !(iface->ker_freq > 0)) {
        err = SPI_ERR_IFACE_NOT_ENABLED;
    } else {
        chip_per_clk_en(iface->per_id);
        chip_per_rst(iface->per_id);
        /* включение удержания пинов при SPE=0, чтобы избежать артефактов
         * при смене конфигурации SPI */
        iface->regs->CFG2 |= CHIP_REGFLD_SPI_CFG2_AFCNTR;
        /* при программном управлении, чтобы не было ошибки занятости шины,
         * выбираем состояние SS всегда высоким */
        iface->regs->CR1  |= CHIP_REGFLD_SPI_CR1_SSI;

#if SPI_IFACE1_EN
        if (iface_num == SPI_IFACE1_NUM) {
            CHIP_PIN_CONFIG(SPI_CFG_IFACE1_PIN_SCK);
            CHIP_PIN_CONFIG(SPI_CFG_IFACE1_PIN_MOSI);
            CHIP_PIN_CONFIG(SPI_CFG_IFACE1_PIN_MISO);
        }
#endif
#if SPI_IFACE2_EN
        if (iface_num == SPI_IFACE2_NUM) {
            CHIP_PIN_CONFIG(SPI_CFG_IFACE2_PIN_SCK);
            CHIP_PIN_CONFIG(SPI_CFG_IFACE2_PIN_MOSI);
            CHIP_PIN_CONFIG(SPI_CFG_IFACE2_PIN_MISO);
        }
#endif
#if SPI_IFACE3_EN
        if (iface_num == SPI_IFACE3_NUM) {
            CHIP_PIN_CONFIG(SPI_CFG_IFACE3_PIN_SCK);
            CHIP_PIN_CONFIG(SPI_CFG_IFACE3_PIN_MOSI);
            CHIP_PIN_CONFIG(SPI_CFG_IFACE3_PIN_MISO);
        }
#endif
#if SPI_IFACE4_EN
        if (iface_num == SPI_IFACE4_NUM) {
            CHIP_PIN_CONFIG(SPI_CFG_IFACE4_PIN_SCK);
            CHIP_PIN_CONFIG(SPI_CFG_IFACE4_PIN_MOSI);
            CHIP_PIN_CONFIG(SPI_CFG_IFACE4_PIN_MISO);
        }
#endif
#if SPI_IFACE5_EN
        if (iface_num == SPI_IFACE3_NUM) {
            CHIP_PIN_CONFIG(SPI_CFG_IFACE5_PIN_SCK);
            CHIP_PIN_CONFIG(SPI_CFG_IFACE5_PIN_MOSI);
            CHIP_PIN_CONFIG(SPI_CFG_IFACE5_PIN_MISO);
        }
#endif
#if SPI_IFACE6_EN
        if (iface_num == SPI_IFACE3_NUM) {
            CHIP_PIN_CONFIG(SPI_CFG_IFACE6_PIN_SCK);
            CHIP_PIN_CONFIG(SPI_CFG_IFACE6_PIN_MOSI);
            CHIP_PIN_CONFIG(SPI_CFG_IFACE6_PIN_MISO);
        }
#endif
    }
    return err;
}

void spi_port_iface_close(uint8_t iface_num) {    
    const t_spi_iface_info *iface = &f_ifaces[iface_num];
#if SPI_IFACE1_EN
        if (iface_num == SPI_IFACE1_NUM) {
            CHIP_PIN_CONFIG_DIS(SPI_CFG_IFACE1_PIN_SCK);
            CHIP_PIN_CONFIG_DIS(SPI_CFG_IFACE1_PIN_MOSI);
            CHIP_PIN_CONFIG_DIS(SPI_CFG_IFACE1_PIN_MISO);
        }
#endif
#if SPI_IFACE2_EN
        if (iface_num == SPI_IFACE2_NUM) {
            CHIP_PIN_CONFIG_DIS(SPI_CFG_IFACE2_PIN_SCK);
            CHIP_PIN_CONFIG_DIS(SPI_CFG_IFACE2_PIN_MOSI);
            CHIP_PIN_CONFIG_DIS(SPI_CFG_IFACE2_PIN_MISO);
        }
#endif
#if SPI_IFACE3_EN
        if (iface_num == SPI_IFACE3_NUM) {
            CHIP_PIN_CONFIG_DIS(SPI_CFG_IFACE3_PIN_SCK);
            CHIP_PIN_CONFIG_DIS(SPI_CFG_IFACE3_PIN_MOSI);
            CHIP_PIN_CONFIG_DIS(SPI_CFG_IFACE3_PIN_MISO);
        }
#endif
#if SPI_IFACE4_EN
        if (iface_num == SPI_IFACE4_NUM) {
            CHIP_PIN_CONFIG_DIS(SPI_CFG_IFACE4_PIN_SCK);
            CHIP_PIN_CONFIG_DIS(SPI_CFG_IFACE4_PIN_MOSI);
            CHIP_PIN_CONFIG_DIS(SPI_CFG_IFACE4_PIN_MISO);
        }
#endif
#if SPI_IFACE5_EN
        if (iface_num == SPI_IFACE3_NUM) {
            CHIP_PIN_CONFIG_DIS(SPI_CFG_IFACE5_PIN_SCK);
            CHIP_PIN_CONFIG_DIS(SPI_CFG_IFACE5_PIN_MOSI);
            CHIP_PIN_CONFIG_DIS(SPI_CFG_IFACE5_PIN_MISO);
        }
#endif
#if SPI_IFACE6_EN
        if (iface_num == SPI_IFACE3_NUM) {
            CHIP_PIN_CONFIG_DIS(SPI_CFG_IFACE6_PIN_SCK);
            CHIP_PIN_CONFIG_DIS(SPI_CFG_IFACE6_PIN_MOSI);
            CHIP_PIN_CONFIG_DIS(SPI_CFG_IFACE6_PIN_MISO);
        }
#endif


    /* возврат регистров в начальные значения */
    iface->regs->CR1  &= CHIP_REGMSK_SPI_CR1_RESERVED;
    iface->regs->CFG1  = (iface->regs->CR2 & CHIP_REGMSK_SPI_CFG1_RESERVED) | 0x00070007; /* set reset value */
    iface->regs->CFG2 &= CHIP_REGMSK_SPI_CFG2_RESERVED;

    chip_per_clk_dis(f_ifaces[iface_num].per_id);
}


t_spi_err spi_port_dev_check(const t_spi_dev *dev) {
    const t_spi_iface_info *iface = &f_ifaces[dev->iface_num];
    return dev->wrdlen > iface->max_dlen  ? SPI_ERR_UNSUP_DATA_LEN : SPI_ERR_OK;
}

void spi_port_dev_activate(const t_spi_dev *dev, const t_spi_dev *prev_dev) {
   const t_spi_iface_info *iface = &f_ifaces[dev->iface_num];
   if (!prev_dev || (dev->wrdlen != prev_dev->wrdlen) || (dev->baudrate != prev_dev->baudrate) || (dev->flags != prev_dev->flags)) {

       unsigned div = (iface->ker_freq + dev->baudrate - 1) / dev->baudrate;
       unsigned br_val = CHIP_REGFLDVAL_SPI_CFG1_MBR_DIV256;

       for (unsigned i = 0; i < sizeof (f_clk_div) / sizeof (f_clk_div[0]); ++i) {
            if (f_clk_div[i] >= div) {
                br_val = i;
                break;
            }
       }

       iface->regs->CR1 &= ~CHIP_REGFLD_SPI_CR1_SPE;

       iface->regs->CFG1 = (iface->regs->CFG1 & CHIP_REGMSK_SPI_CFG1_RESERVED)
               | LBITFIELD_SET(CHIP_REGFLD_SPI_CFG1_MBR, br_val)
               | LBITFIELD_SET(CHIP_REGFLD_SPI_CFG1_FTHLV, 0)
               | LBITFIELD_SET(CHIP_REGFLD_SPI_CFG1_DSIZE, dev->wrdlen-1);

       iface->regs->CFG2 = (iface->regs->CFG2 & CHIP_REGMSK_SPI_CFG2_RESERVED)
               | CHIP_REGFLD_SPI_CFG2_AFCNTR
               | ((dev->flags & SPI_FLAG_CS_HWCTL) ? CHIP_REGFLD_SPI_CFG2_SSOE
                                                   : CHIP_REGFLD_SPI_CFG2_SSM)
               | ((dev->flags & SPI_FLAG_CPOL) ? CHIP_REGFLD_SPI_CFG2_CPOL : 0)
               | ((dev->flags & SPI_FLAG_CPHA) ? CHIP_REGFLD_SPI_CFG2_CPHA : 0)
               | CHIP_REGFLD_SPI_CFG2_MASTER;

       iface->regs->CR1 |= CHIP_REGFLD_SPI_CR1_SPE;
   }
}

void spi_port_exchange_start(const t_spi_dev *dev) {
    const t_spi_iface_info *iface = &f_ifaces[dev->iface_num];
    f_onfly_bytes[dev->iface_num] = 0;
    chip_dmb();
    iface->regs->CR1 |= CHIP_REGFLD_SPI_CR1_CSTART;
}

void spi_port_exchange_finish(const t_spi_dev *dev) {
    chip_dmb();
}

int spi_port_tx_rdy(const t_spi_dev *dev) {
    const t_spi_iface_info *iface = &f_ifaces[dev->iface_num];
    return (iface->regs->SR & CHIP_REGFLD_SPI_SR_TXP) && (f_onfly_bytes[dev->iface_num] < iface->fifo_len);
}
int spi_port_rx_rdy(const t_spi_dev *dev) {
    const t_spi_iface_info *iface = &f_ifaces[dev->iface_num];
    return iface->regs->SR & CHIP_REGFLD_SPI_SR_RXP;
}

int spi_port_busy(const t_spi_dev *dev) {
    const t_spi_iface_info *iface = &f_ifaces[dev->iface_num];
    const  uint32_t sr = iface->regs->SR;
    return (sr & CHIP_REGFLD_SPI_SR_TXC) == 0;
}

void spi_port_tx_word(const t_spi_dev *dev, uint32_t wrd) {
    const t_spi_iface_info *iface = &f_ifaces[dev->iface_num];
    if (dev->wrdlen > 16) {
        iface->regs->TXDR32 = wrd;
        f_onfly_bytes[dev->iface_num] += 4;
    } else if (dev->wrdlen > 8) {
        iface->regs->TXDR16 = wrd;
        f_onfly_bytes[dev->iface_num] += 2;
    } else {
        iface->regs->TXDR8 = wrd;
        f_onfly_bytes[dev->iface_num] += 1;
    }
}
uint32_t spi_port_rx_word(const t_spi_dev *dev) {

    const t_spi_iface_info *iface = &f_ifaces[dev->iface_num];
    uint32_t ret;
    if (dev->wrdlen > 16) {
        ret = iface->regs->RXDR32;
        f_onfly_bytes[dev->iface_num] -= 4;
    } else if (dev->wrdlen > 8) {
        ret = iface->regs->RXDR16;
        f_onfly_bytes[dev->iface_num] -= 2;
    } else {
        ret = iface->regs->RXDR8;
        f_onfly_bytes[dev->iface_num] -= 1;
    }
    return ret;
}
