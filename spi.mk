SPI_MAKEFILE = $(abspath $(lastword $(MAKEFILE_LIST)))
SPI_DIR := $(dir $(SPI_MAKEFILE))

SPI_TARGET ?= $(TARGET_CPU)

SPI_SRC := $(SPI_DIR)/spi.c
SPI_INC_DIRS := $(SPI_DIR)

ifeq ($(SPI_TARGET), stm32_spi_v1)
    SPI_PORT_DIR = $(SPI_DIR)/stm32_spi_v1
    SPI_PORT_SRC = $(SPI_PORT_DIR)/stm32_spi_v1.c
else ifeq ($(SPI_TARGET), stm32h7xx)
    SPI_PORT_DIR = $(SPI_DIR)/stm32h7xx
    SPI_PORT_SRC = $(SPI_PORT_DIR)/stm32h7xx_spi.c
else
    $(error spi err: port architecture should be specified in SPI_TARGET or TARGET_CPU variable)
endif

SPI_SRC += $(SPI_PORT_SRC)
SPI_INC_DIRS += $(SPI_PORT_DIR)
