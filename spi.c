#include "chip.h"
#include "spi.h"
#include "spi_port.h"
#include "spi_config.h"
#include <stddef.h>

typedef struct {
    unsigned dev_cntr;
    const t_spi_dev *cur_dev;
} t_spi_state;

static t_spi_state f_iface_states[CHIP_DEV_SPI_CNT];


static void spi_wait_not_busy(const t_spi_dev *dev) {
    while (spi_port_busy(dev)) {
        continue;
    }
}

int spi_dev_activate(const t_spi_dev *dev) {
    t_spi_state *spi_state = &f_iface_states[dev->iface_num];

    spi_wait_not_busy(dev);

    spi_port_dev_activate(dev, spi_state->cur_dev);

    spi_state->cur_dev = dev;

    return 0;
}



static t_spi_err spi_iface_init(uint8_t iface_num) {
    f_iface_states[iface_num].cur_dev = NULL;
    return spi_port_iface_init(iface_num);
}

static void spi_iface_close(uint8_t iface_num) {
    f_iface_states[iface_num].cur_dev = NULL;
    spi_port_iface_close(iface_num);
}



t_spi_err spi_dev_init(const t_spi_dev *dev) {
    t_spi_err err = SPI_ERR_OK;
    t_spi_state *state = &f_iface_states[dev->iface_num];

    if (dev->iface_num >= CHIP_DEV_SPI_CNT) {
        err = SPI_ERR_INVALID_IFACE_NUM;
    } else if (dev->wrdlen > SPI_PORT_MAX_DATA_LEN) {
        err = SPI_ERR_UNSUP_DATA_LEN;
    } else {
        err = spi_port_dev_check(dev);
    }


    if ((err == SPI_ERR_OK) && (state->dev_cntr == 0)) {
        err = spi_iface_init(dev->iface_num);
    }

    if (err == SPI_ERR_OK) {
        ++state->dev_cntr;

#if SPI_PORT_CS_HWCTL_MAX_CNT > 0
        if (dev->flags & SPI_FLAG_CS_HWCTL) {
            CHIP_PIN_CONFIG(dev->cs_pin);
        } else {
#endif
            CHIP_PIN_CONFIG_OUT(dev->cs_pin, 1);
#if SPI_PORT_CS_HWCTL_MAX_CNT > 0
        }
#endif
    }
    return err;
}

void spi_dev_close(const t_spi_dev *dev) {
    t_spi_state *state = &f_iface_states[dev->iface_num];
    if (state->dev_cntr > 0) {
        --state->dev_cntr;
        if (state->dev_cntr == 0) {
            spi_iface_close(dev->iface_num);
        }
    }
}

void spi_exchange_start(const t_spi_dev *dev) {
    if (f_iface_states[dev->iface_num].cur_dev != NULL) {
        spi_wait_not_busy(f_iface_states[dev->iface_num].cur_dev);
    }

    if (f_iface_states[dev->iface_num].cur_dev != dev) {
        spi_dev_activate(dev);
    }

    if (dev->exchange_start_cb)
        dev->exchange_start_cb(dev);

    spi_port_exchange_start(dev);

    if (!(dev->flags & SPI_FLAG_CS_HWCTL))
        CHIP_PIN_OUT(dev->cs_pin, 0);
}

void spi_exchange_finish(const t_spi_dev *dev) {    
    spi_wait_not_busy(dev);

    if (!(dev->flags & SPI_FLAG_CS_HWCTL))
        CHIP_PIN_OUT(dev->cs_pin, 1);

    spi_port_exchange_finish(dev);

    if (dev->exchange_finish_cb)
        dev->exchange_finish_cb(dev);
}


uint32_t spi_exchange_exec_word(const t_spi_dev *dev, uint32_t wrd) {
    uint32_t rx_wrd;
    spi_exchange_start(dev);
    rx_wrd = spi_exchange_step_word(dev, wrd);
    spi_exchange_finish(dev);
    return rx_wrd;
}


uint32_t spi_exchange_step_word(const t_spi_dev *dev, uint32_t wrd) {
    spi_port_tx_word(dev, wrd);
    while (!spi_port_rx_rdy(dev)) {
        continue;
    }
    return spi_port_rx_word(dev);
}

void spi_exchange_step_array(const t_spi_dev *dev, const void *txbuf, void *rxbuf, uint32_t wrds_cnt, void (*cb_func)(void)) {
    const uint8_t *txptr = (const uint8_t *)txbuf;
    uint8_t *rxptr = (uint8_t *)rxbuf;
    uint32_t rx_wrd_cnt = 0;
    uint32_t tx_wrd_cnt = 0;

    while (rx_wrd_cnt < wrds_cnt) {
        if ((tx_wrd_cnt < wrds_cnt) && spi_port_tx_rdy(dev)) {
            uint32_t wrd = 0xFF;
            if (txptr != NULL) {
                wrd = *txptr++;
#if SPI_PORT_MAX_DATA_LEN > 8
                if (dev->wrdlen > 8)
                    wrd |= (uint32_t)(*txptr++) << 8;
#endif
#if SPI_PORT_MAX_DATA_LEN > 16
                if (dev->wrdlen > 16)
                    wrd |= (uint32_t)(*txptr++) << 16;
#endif
#if SPI_PORT_MAX_DATA_LEN > 24
                if (dev->wrdlen > 24)
                    wrd |= (uint32_t)(*txptr++) << 24;
#endif
            }
            spi_port_tx_word(dev, wrd);
            tx_wrd_cnt++;
        }

        if (spi_port_rx_rdy(dev)) {
            uint32_t rx_wrd = spi_port_rx_word(dev);
            rx_wrd_cnt++;
            if (rxptr != NULL) {
                *rxptr++ = rx_wrd & 0xFF;
#if SPI_PORT_MAX_DATA_LEN > 8
                if (dev->wrdlen > 8) {
                    *rxptr++ = (rx_wrd >> 8) & 0xFF;
                }
#endif
#if SPI_PORT_MAX_DATA_LEN > 16
                if (dev->wrdlen > 16) {
                    *rxptr++ = (rx_wrd >> 16) & 0xFF;
                }
#endif
#if SPI_PORT_MAX_DATA_LEN > 24
                if (dev->wrdlen > 24) {
                    *rxptr++ = (rx_wrd >> 24) & 0xFF;
                }
#endif
            }
        }

        if (cb_func != NULL) {
            cb_func();
        }
    }
}

void spi_exchange_exec_array(const t_spi_dev *dev, const void *txbuf, void *rxbuf, uint32_t wrds_cnt, void (*cb_func)(void)) {
    spi_exchange_start(dev);
    spi_exchange_step_array(dev, txbuf, rxbuf, wrds_cnt, cb_func);
    spi_exchange_finish(dev);
}



