#include "spi_port.h"
#include "chip.h"
#include <stddef.h>

typedef struct {
    CHIP_REGS_SPI_T *regs;
    int  per_id;
    unsigned  ker_freq;
} t_spi_iface_info;

static const t_spi_iface_info f_ifaces[] = {
#if SPI_IFACE1_EN
    {CHIP_REGS_SPI1, CHIP_PER_ID_SPI1, CHIP_CLK_SPI1_FREQ},
#else
    {NULL, 0, 0},
#endif
#if SPI_IFACE2_EN
    {CHIP_REGS_SPI2, CHIP_PER_ID_SPI2, CHIP_CLK_SPI2_FREQ},
#else
    {NULL, 0, 0},
#endif
#if SPI_IFACE3_EN
    {CHIP_REGS_SPI3, CHIP_PER_ID_SPI3, CHIP_CLK_SPI3_FREQ},
#else
    {NULL, 0, 0},
#endif
};

static uint8_t f_onfly_bytes[CHIP_DEV_SPI_CNT];

static const uint16_t f_clk_div[] = {2, 4, 8, 16, 32, 64, 128, 256};

t_spi_err spi_port_iface_init(uint8_t iface_num) {
    t_spi_err err = SPI_ERR_OK;
    const t_spi_iface_info *iface = &f_ifaces[iface_num];

    if ((iface->regs == NULL) || !(iface->ker_freq > 0)) {
        err = SPI_ERR_IFACE_NOT_ENABLED;
    } else {
        chip_per_clk_en(iface->per_id);
        chip_per_rst(iface->per_id);
#if SPI_IFACE1_EN
        if (iface_num == SPI_IFACE1_NUM) {
            CHIP_PIN_CONFIG(SPI_CFG_IFACE1_PIN_SCK);
            CHIP_PIN_CONFIG(SPI_CFG_IFACE1_PIN_MOSI);
            CHIP_PIN_CONFIG(SPI_CFG_IFACE1_PIN_MISO);
        }
#endif
#if SPI_IFACE2_EN
        if (iface_num == SPI_IFACE2_NUM) {
            CHIP_PIN_CONFIG(SPI_CFG_IFACE2_PIN_SCK);
            CHIP_PIN_CONFIG(SPI_CFG_IFACE2_PIN_MOSI);
            CHIP_PIN_CONFIG(SPI_CFG_IFACE2_PIN_MISO);
        }
#endif
#if SPI_IFACE3_EN
        if (iface_num == SPI_IFACE3_NUM) {
            CHIP_PIN_CONFIG(SPI_CFG_IFACE3_PIN_SCK);
            CHIP_PIN_CONFIG(SPI_CFG_IFACE3_PIN_MOSI);
            CHIP_PIN_CONFIG(SPI_CFG_IFACE3_PIN_MISO);
        }
#endif
    }
    return err;
}

void spi_port_iface_close(uint8_t iface_num) {
    const t_spi_iface_info *iface = &f_ifaces[iface_num];
    iface->regs->CR1 = 0;
    iface->regs->CR2 = (iface->regs->CR2 & 0x8000) |  0x0700;
    chip_per_clk_dis(iface->per_id);
}

t_spi_err spi_port_dev_check(const t_spi_dev *dev) {
    return SPI_ERR_OK;
}



void spi_port_dev_activate(const t_spi_dev *dev, const t_spi_dev *prev_dev) {
   const t_spi_iface_info *iface = &f_ifaces[dev->iface_num];
   if (!prev_dev || (dev->wrdlen != prev_dev->wrdlen) || (dev->baudrate != prev_dev->baudrate) || (dev->flags != prev_dev->flags)) {
       iface->regs->CR1 &= ~CHIP_REGFLD_SPI_CR1_SPE;

       unsigned div = (iface->ker_freq + dev->baudrate - 1) / dev->baudrate;
       unsigned br_val = CHIP_REGFLDVAL_SPI_CR1_BR_DIV256;

       for (unsigned i = 0; i < sizeof (f_clk_div) / sizeof (f_clk_div[0]); ++i) {
            if ((f_clk_div[i] >= div) && !((iface->ker_freq / f_clk_div[i]) > CHIP_CLK_SPI_FREQ_MAX)) {
                br_val = i;
                break;
            }
       }

       iface->regs->CR1 = LBITFIELD_SET(CHIP_REGFLD_SPI_CR1_BR, br_val)
               | ((dev->flags & SPI_FLAG_CPOL) ? CHIP_REGFLD_SPI_CR1_CPOL : 0)
               | ((dev->flags & SPI_FLAG_CPHA) ? CHIP_REGFLD_SPI_CR1_CPHA : 0)
               | ((dev->flags & SPI_FLAG_CS_HWCTL) ? 0 : (CHIP_REGFLD_SPI_CR1_SSM | CHIP_REGFLD_SPI_CR1_SSI))
               | CHIP_REGFLD_SPI_CR1_MSTR               
#if !CHIP_SUPPORT_SPI_CUSTOM_DATA_SIZE
               | LBITFIELD_SET(CHIP_REGFLD_SPI_CR1_DFF, dev->wrdlen == 16 ? CHIP_REGFLDVAL_SPI_CR1_DFF_16BIT : CHIP_REGFLDVAL_SPI_CR1_DFF_8BIT)
#endif
               ;

       iface->regs->CR2 = 0
       #if CHIP_SUPPORT_SPI_CUSTOM_DATA_SIZE
               | LBITFIELD_SET(CHIP_REGFLD_SPI_CR2_DS, dev->wrdlen-1)
       #endif
       #if CHIP_SUPPORT_SPI_FIFO
               | LBITFIELD_SET(CHIP_REGFLD_SPI_CR2_FRXTH,
                               ((dev->wrdlen > 8) ? CHIP_REGFLDVAL_SPI_CR2_FRXTH_16BIT :
                                                    CHIP_REGFLDVAL_SPI_CR2_FRXTH_8BIT))
       #endif
               | ((dev->flags & SPI_FLAG_CS_HWCTL) ? CHIP_REGFLD_SPI_CR2_SS_OE : 0);

       f_onfly_bytes[dev->iface_num] = 0;

       iface->regs->CR1 |= CHIP_REGFLD_SPI_CR1_SPE;
   }
}

void spi_port_exchange_start(const t_spi_dev *dev) {

}

void spi_port_exchange_finish(const t_spi_dev *dev) {

}

int spi_port_tx_rdy(const t_spi_dev *dev) {
    const t_spi_iface_info *iface = &f_ifaces[dev->iface_num];
#if CHIP_SUPPORT_SPI_FIFO
    return LBITFIELD_GET(iface->regs->SR, CHIP_REGFLD_SPI_SR_FTLVL) != CHIP_REGFLDVAL_SPI_SR_FLVL_FULL;
#else
    uint32_t sr = iface->regs->SR;
    return ((sr & CHIP_REGFLD_SPI_SR_TXE) != 0) && (f_onfly_bytes[dev->iface_num] == 0);
#endif
}
int spi_port_rx_rdy(const t_spi_dev *dev) {
    const t_spi_iface_info *iface = &f_ifaces[dev->iface_num];
#if CHIP_SUPPORT_SPI_FIFO
    return LBITFIELD_GET(iface->regs->SR, CHIP_REGFLD_SPI_SR_FRLVL) != CHIP_REGFLDVAL_SPI_SR_FLVL_EMPTY;
#else
    uint32_t sr = iface->regs->SR;
    return (sr & CHIP_REGFLD_SPI_SR_RXNE) != 0;
#endif
}

int spi_port_busy(const t_spi_dev *dev) {
    const t_spi_iface_info *iface = &f_ifaces[dev->iface_num];
    const  uint32_t sr = iface->regs->SR;
#if CHIP_SUPPORT_SPI_FIFO
    return (LBITFIELD_GET(sr, CHIP_REGFLD_SPI_SR_FTLVL) != CHIP_REGFLDVAL_SPI_SR_FLVL_EMPTY)
            || (sr & CHIP_REGFLD_SPI_SR_BSY);
#else
    return (sr & CHIP_REGFLD_SPI_SR_BSY) != 0;
#endif
}

void spi_port_tx_word(const t_spi_dev *dev, uint32_t wrd) {
    const t_spi_iface_info *iface = &f_ifaces[dev->iface_num];
    if (dev->wrdlen > 8) {
        iface->regs->DR16 = wrd;
    } else {
        iface->regs->DR8 = wrd;
    }
    ++f_onfly_bytes[dev->iface_num];
}
uint32_t spi_port_rx_word(const t_spi_dev *dev) {
    const t_spi_iface_info *iface = &f_ifaces[dev->iface_num];
    uint32_t res =  dev->wrdlen > 8 ? iface->regs->DR16 : iface->regs->DR8;
    --f_onfly_bytes[dev->iface_num];
    return res;
}
