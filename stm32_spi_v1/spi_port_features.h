#ifndef SPI_PORT_FEATURES_H
#define SPI_PORT_FEATURES_H

#include "chip.h"
#include "spi_config.h"

#define SPI_PORT_CS_HWCTL_MAX_CNT    1
#define SPI_PORT_SUPPORT_DMA         0
#define SPI_PORT_MAX_DATA_LEN        16

#define SPI_IFACE1_NUM         0
#define SPI_IFACE2_NUM         1
#define SPI_IFACE3_NUM         2

#ifdef SPI_CFG_IFACE1_EN
    #define SPI_IFACE1_EN SPI_CFG_IFACE1_EN
#else
    #define SPI_IFACE1_EN 0
#endif

#if defined SPI_CFG_IFACE2_EN && (CHIP_DEV_SPI_CNT >= 2)
    #define SPI_IFACE2_EN SPI_CFG_IFACE2_EN
#else
    #define SPI_IFACE2_EN 0
#endif

#if defined SPI_CFG_IFACE3_EN && (CHIP_DEV_SPI_CNT >= 3)
    #define SPI_IFACE3_EN SPI_CFG_IFACE3_EN
#else
    #define SPI_IFACE3_EN 0
#endif




#endif // SPI_PORT_FEATURES_H

