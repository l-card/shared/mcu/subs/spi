#ifndef SPI_PORT_H
#define SPI_PORT_H

#include "spi.h"


t_spi_err spi_port_iface_init(uint8_t iface_num);
void spi_port_iface_close(uint8_t iface_num);
t_spi_err spi_port_dev_check(const t_spi_dev *dev);
void spi_port_dev_activate(const t_spi_dev *dev, const t_spi_dev *prev_dev);
void spi_port_exchange_start(const t_spi_dev *dev);
void spi_port_exchange_finish(const t_spi_dev *dev);
int spi_port_tx_rdy(const t_spi_dev *dev);
int spi_port_rx_rdy(const t_spi_dev *dev);
int spi_port_busy(const t_spi_dev *dev);
void spi_port_tx_word(const t_spi_dev *dev, uint32_t wrd);
uint32_t spi_port_rx_word(const t_spi_dev *dev);



#endif // SPI_PORT_H
