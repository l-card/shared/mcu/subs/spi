# Описание репозитория

Репозиторий содержит файлы для общей реализации интерфейса обмена с подчиненными устройствами по SPI-порту без привязки к конкретному контроллеру.

# Использование в проекте

Репозиторий предназначен для использования в проектах, собираемых с помощью утилиты **make** с использованием компилятора **gcc**.

1. Репозиторий требует использования репозитория **chip** в проекте, т.к. управление пинами, описания регистров, определение частот и управление разрешением интерфейсов использует функции, определенные в данном репозитории. Также репозиторий использует переменную **CHIP_TARGET** для определения используемого порта, поэтому **chip.mk** должен включаться заранее в **Makefile** проекта.

2. В **Makefile**-файле проекта необходимо включить файл  **spi.mk**:
3. После включения будут установлены следующие переменные, которые можно использовать в проекте:


- **SPI_SRC** — все исходные коды на С для сборки с проектом.
- **SPI_INC_DIRS** — директории с заголовочными фалами, которые должны быть добавлены в список директорий для поиска заголовков проекта.

4. В проекте должен быть создан файл конфигурации **spi_config.h** в произвольной директории, добавленной в список директорий поиска заголовков проекта. В данном файле с помощью определений препроцессора должны быть заданы параметры инициализации интерфейсов. Для каждого порта под конкретный контроллер могут быть свои параметры. Пример данного файла для каждого порта находится в файле **<директория порта>/_spi_config.h**.


